import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

import Qt.vgf.mainlogic 1.0
import "."

Item {
    id: root
    property bool all_enabled: MainLogic.connected
    width: 240
    height: labelCurrentT.height + labelPresetT.height + led.height + labelZone.height + switchStop.height + column1.spacing*4 + 5
//    property int zoneNumber: zone

    Rectangle {
        height: parent.height
        width: parent.width
        color: "transparent"
        border.color: "red"
        border.width: 2
        enabled: all_enabled

        Column {
            id: column1
            height: parent.height
            width: parent.width
            anchors.topMargin: 20
            spacing: 8
            property int labelFontSize: 26

            Label {
                id: labelCurrentT
                text: qsTr("Текущая: ") + modelData.CurrentT.toFixed(1)

                color: "white"
                font.pixelSize: parent.labelFontSize
                renderType: Text.NativeRendering

                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                id: labelPresetT
                text: qsTr("Задание: ") + modelData.PresetT.toFixed(1)
                color: "white"
                font.pixelSize: parent.labelFontSize
                renderType: Text.NativeRendering

                anchors.horizontalCenter: parent.horizontalCenter
            }

            Rectangle {
                id: led
                width: 35
                smooth: true
                height: width
                color: {
                    if (index > 11)
                        "red";
                    else
                        "green";
                }

                border.width: 3
                border.color: "aqua"

                radius: width*0.5
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Switch {
                id: switchStop
                height: 40
                width: 160
                anchors.horizontalCenter: parent.horizontalCenter
                checked: false

                style: StyleSwitch {}
                onClicked: {
                    console.log("Switch " + (index + 1) + " changed to " + checked)
                    modelData.SwitchChanged(index, checked)
                }
            }

            Label {
                id: labelZone
                anchors.horizontalCenter: parent.horizontalCenter

                text: qsTr("Zone ") + (modelData.ZoneNumber + 1)
                color: "white"
                font.pixelSize: parent.labelFontSize
            }
        }
    }
}
