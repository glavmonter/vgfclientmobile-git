import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import Qt.vgf.mainlogic 1.0

Item {
    width: parent.width
    height: parent.height

    Column {
        spacing: 20
        width: parent.width
        height: parent.height
        anchors.top: parent.top
        anchors.topMargin: 20

        GridView {
            clip: true
            width: parent.width
            height: parent.height

            cellWidth: 200+60
            cellHeight: 220

            model: MainLogic.ModelZone
            delegate: Zone {}

        }
    }
}

