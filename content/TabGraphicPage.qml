
import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1
import QuickVgf 1.0

Item {
    width: parent.width
    height: parent.height

    QuickVgfFurnace {
        anchors.top: parent.top
        anchors.topMargin: 10
        id: plot
        anchors.fill: parent
    }
}
