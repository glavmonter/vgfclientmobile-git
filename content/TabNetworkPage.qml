import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import Qt.vgf.mainlogic 1.0

Item {
    id: item1
//    width: parent.width
//    height: parent.height

    width: 800
    height: 1000
    property bool connected: MainLogic.connected

    Item {
        id: root
        property int numsToEmc: 10
        property bool emcOpacity: !(numsToEmc > 0)


        anchors.top: parent.top
        anchors.topMargin: 20
        width: 800
        height: 800
        anchors.horizontalCenter: parent.horizontalCenter
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 20

            Rectangle {
                border.width: 2
                border.color: "red"
                color: "transparent"
                width: column1.width + 10
                height: ipAddressFields.height + row.height + column1.spacing*2

                Column {
                    id: column1
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 20

                    spacing: 40

                    TextField {
                        id: ipAddressFields
                        anchors.margins: 20
                        text: MainLogic.ipAddress
                        anchors.horizontalCenter: parent.horizontalCenter
                        style: StyleTextField {}
                        horizontalAlignment: TextInput.AlignHCenter

                        validator:RegExpValidator
                        {
                            regExp:/^(([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))\.){3}([01]?[0-9]?[0-9]|2([0-4][0-9]|5[0-5]))$/
                        }

                        onEditingFinished: {
                            MainLogic.ipAddress = text
                        }
                    }

                    Row {
                        id: row
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 5

                        Label {
                            anchors.verticalCenter: parent.verticalCenter
                            text: qsTr("Подключение")
                            color: "white"
                            font.pixelSize: 23
                        }

                        Switch {
                            height: 40
                            width: 160
                            onClicked: MainLogic.switchConnectedChanged(checked)

                            style: StyleSwitch {}
                        }
                    }

                    Button {
                        id: btnExit

                        width: 370
                        height: 50
                        text: qsTr("Выход")
                        anchors.horizontalCenter: parent.horizontalCenter
                        style: StyleButton {}

                        onClicked: Qt.quit()
                    }
                }
            }

            Rectangle {
                border.width: 2
                border.color: "red"
                color: "transparent"
                width: btnExpertMode.width + 20
                height: resetButton.height + powerSwitch.height + zoneSwitch.height + btnExpertMode.height + gridEmc.rowSpacing*6

                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    spacing: 20

                    Button {
                        id: btnExpertMode
                        width: 370
                        height: 50

                        anchors.horizontalCenter: parent.horizontalCenter
                        style: StyleButton {}

                        text: qsTr("Режим Бога: " + root.numsToEmc)
                        onClicked: {
                            if (root.numsToEmc > 0) {
                                root.numsToEmc -= 1
                            }
                        }
                    }

                    Rectangle {
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: parent.width - 50
                        height: 2
                        color: "red"
                    }

                    Item {
                        id: emcControls
                        width: btnExpertMode.width
                        height: 200
                        anchors.horizontalCenter: parent.horizontalCenter
                        opacity: root.emcOpacity ? 1 : 0
                        Behavior on opacity {NumberAnimation {}}
                        enabled: root.emcOpacity

                        Column {
                            id: column2
                            width: btnExpertMode.width
                            anchors.top: parent.top
                            anchors.topMargin: 10
                            spacing: 20

                            height: 200
                            anchors.horizontalCenter: parent.horizontalCenter

                            Button {
                                id: resetButton
                                width: 200
                                height: 50
                                anchors.horizontalCenter: parent.horizontalCenter
                                style: StyleButton {}

                                text: qsTr("Сброс")
                                onClicked: {
                                    root.numsToEmc = 10
                                }
                            }

                            Grid {
                                id: gridEmc
                                anchors.horizontalCenter: parent.horizontalCenter
                                columns: 2
                                columnSpacing: 3
                                rowSpacing: 20

                                verticalItemAlignment: Grid.AlignVCenter
                                horizontalItemAlignment: Grid.AlignRight

                                Label {
                                    text: qsTr("Питание")
                                    color: "white"
                                    font.pixelSize: 23
                                }

                                Switch {
                                    id: powerSwitch
                                    height: 40
                                    width: 160
                                    style: StyleSwitch {}
                                }


                                Label {
                                    text: qsTr("Остановка зон")
                                    color: "white"
                                    font.pixelSize: 23
                                }

                                Switch {
                                    id: zoneSwitch
                                    height: 40
                                    width: 160
                                    style: StyleSwitch {}
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
