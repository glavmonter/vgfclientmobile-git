import QtQuick 2.0
import QtQuick.Controls.Styles 1.2

Component {
    id: switchStyle
    SwitchStyle {
        groove: Rectangle {
            implicitHeight: parent.parent.parent.parent.height
            implicitWidth: parent.parent.parent.parent.width

            Rectangle {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.bottom: parent.bottom

                width: parent.width/2 - 2
                height: 20
                anchors.margins: 2

                color: control.checked ? "#468bb7" : "#222"
                Behavior on color {ColorAnimation {} }

                Text {
                    font.pixelSize: 23
                    color: "white"
                    anchors.centerIn: parent
                    text: "|"
                }
            }

            Item {
                width: parent.width/2
                height: parent.height
                anchors.right: parent.right
                Text {
                    font.pixelSize: 23
                    color: "white"
                    anchors.centerIn: parent
                    text: "O"
                }
            }

            color: "#222"
            border.color: "#444"
            border.width: 2
        }

        handle: Rectangle {
            width: parent.parent.width/2
            height: control.height
            color: "#444"
            border.color: "#555"
            border.width: 2
        }
    }
}
