import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import Qt.vgf.vgflogic 1.0

import "."

Item {
    id: root
    width: parent.width
    height: parent.height

    property int count: VGFLogic.count

    Component.onCompleted: {
        VGFLogic.text = "Count"
    }

    Column {
        spacing: 20
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 20

        Row {
            spacing: 20
            Switch {
                id: testSwitch
                height: 40
                width: 160
                style: StyleSwitch {}

                onClicked: {
                    VGFLogic.startCounting(testSwitch.checked)
                }
            }

            Label {
                id: labelTimer
                anchors.verticalCenter: parent.verticalCenter
                color: "white"
                font.pixelSize: 23
                renderType: Text.NativeRendering
                text: VGFLogic.text + ": " + count
            }
        }

        Button {
            id: btnButton1
            width: 370
            height: 50
            style: StyleButton {}

            text: qsTr("Нажми для выхода")
            onClicked: {console.log("Quiting..."); Qt.quit()}
        }
    }
}
