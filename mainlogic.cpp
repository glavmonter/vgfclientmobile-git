#include "mainlogic.h"

MainLogic::MainLogic(QObject *parent)
    : QObject(parent)
{
    QSettings settings;
    m_sIpAddress = settings.value("address", "127.0.0.1").toString();
    emit ipAddressChanged();

    for (int i = 0; i < 24; i++) {
        m_lModelZone.append(new ProtoData(i));
    }

    m_pTimer1s = new QTimer();
    connect(m_pTimer1s, &QTimer::timeout, this, &MainLogic::onTimer1sTimeout);

    m_pTcpSocket = new QTcpSocket();
    connect(m_pTcpSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(slotTcpError(QAbstractSocket::SocketError)));
}

MainLogic::~MainLogic()
{
}


void MainLogic::switchConnectedChanged(bool checked)
{
    if (checked) {
        qDebug() << tr("Подключаемся");

        m_pTcpSocket->connectToHost(m_sIpAddress, 9999);
        if (m_pTcpSocket->waitForConnected(5000) == false) {
            setConnected(false);
            return;
        }
        TCP::ClientServer cts;
        cts.set_type(TCP::ClientServer_ClientType_CLIENT_LOCAL);
        std::string str = cts.SerializeAsString();
        m_pTcpSocket->write(str.data(), str.size());

        setConnected(true);
        m_pTimer1s->start(1000);

    } else {
        qDebug() << tr("Отключаемся");
        m_pTimer1s->stop();
        m_pTcpSocket->disconnectFromHost();
        setConnected(false);
    }
}

void MainLogic::onTimer1sTimeout()
{
}

void MainLogic::slotTcpError(QAbstractSocket::SocketError arg)
{
Q_UNUSED(arg);
    qDebug() << tr("TCP error: %1").arg(m_pTcpSocket->errorString());
    setConnected(false);
}


QString MainLogic::ipAddress() const
{
    return m_sIpAddress;
}

void MainLogic::setIpAddress(const QString &arg)
{
    if (m_sIpAddress != arg) {
        m_sIpAddress = arg;
        qDebug() << "Ip address changed: " << arg;
        QSettings settings;
        settings.setValue("address", m_sIpAddress);

        emit ipAddressChanged();
    }
}

QVariant MainLogic::ModelZone() const
{
    return QVariant::fromValue(m_lModelZone);
}

bool MainLogic::connected() const
{
    return m_bConnected;
}

void MainLogic::setConnected(const bool &arg)
{
    if (m_bConnected != arg) {
        m_bConnected = arg;
        emit connectedChanged();
    }
}
