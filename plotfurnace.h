#ifndef PLOTFURNACE_H
#define PLOTFURNACE_H

#include <QQuickPaintedItem>
#include <qwt_plot.h>
#include <qwt_interval.h>
#include <qwt_system_clock.h>
#include <qwt_scale_widget.h>

class QwtPlotCurve;
class QwtPlotMarker;


class PlotFurnace: public QQuickPaintedItem
{
    Q_OBJECT

public:
    PlotFurnace(QQuickItem *parent = 0);
    ~PlotFurnace();

    void paint(QPainter *painter);

    double getPresetT(int zone);
    double getCurrentT(int zone);
    double getAmplouleT(int zone);

public slots:
    void heightChanged();
    void widthChanged();

    void setIntervalLength(double);

    void setPresetT(int zoneIndex, double T);
    void setCurrentT(int zoneIndex, double T);
    void setAmplouleT(int zoneIndex, double T);

    void updateAll();

private:
    QwtPlot *m_pPlot;

    void initGradient();
    void updateCurve();

    void InitData();
    void updateVerticalInterval();

    QwtInterval verticalInterval;
    QVector <double> ZonePosition;
    QVector <double> AmpoulePosition;
    QVector <double> PresetTemps;
    QVector <double> CurrentTemps;
    QVector <double> AmpouleTemps;

    QwtPlotCurve *curveCurrentT;
    QwtPlotCurve *curvePresetT;
    QwtPlotCurve *curveAmpouleT;
};

#endif // PLOTFURNACE_H
