#ifndef PROTODATA_H
#define PROTODATA_H

#include <QObject>
#include <QDebug>
#include "VGFThermo.pb.h"

class ProtoData : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int ZoneNumber READ ZoneNumber WRITE setZoneNumber NOTIFY ZoneNumberChanged)
    Q_PROPERTY(float CurrentT READ CurrentT WRITE setCurrentT NOTIFY CurrentTChanged)
    Q_PROPERTY(float PresetT READ PresetT WRITE setPresetT NOTIFY PresetTChanged)


public:
    ProtoData(QObject *parent = 0);
    ProtoData(int ZoneNumber, float CurrentT = 20.0f, float PresetT = 20.0f, QObject *parent = 0);
    ~ProtoData();

public:
    Q_INVOKABLE void SwitchChanged(int number, bool checked);

    int ZoneNumber() const;
    void setZoneNumber(const int &arg);

    float CurrentT() const;
    void setCurrentT(const float &arg);

    float PresetT() const;
    void setPresetT(const float &arg);

signals:
    void ZoneNumberChanged();
    void CurrentTChanged();
    void PresetTChanged();

private:
    int m_iZoneNumber;
    float m_fCurrentT;
    float m_fPresetT;
};

#endif // PROTODATA_H
