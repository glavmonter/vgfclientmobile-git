#include "protodata.h"

ProtoData::ProtoData(QObject *parent)
    : QObject(parent)
{
}

ProtoData::ProtoData(int ZoneNumber, float CurrentT, float PresetT, QObject *parent)
    : m_iZoneNumber(ZoneNumber), m_fCurrentT(CurrentT), m_fPresetT(PresetT), QObject(parent)
{
}

ProtoData::~ProtoData()
{

}

void ProtoData::SwitchChanged(int number, bool checked)
{
    qDebug() << tr("Zone %1 changed to %2").arg(number + 1).arg(checked);
}


int ProtoData::ZoneNumber() const
{
    return m_iZoneNumber;
}

void ProtoData::setZoneNumber(const int &arg)
{
    if (m_iZoneNumber != arg) {
        m_iZoneNumber = arg;
        emit ZoneNumberChanged();
    }
}

float ProtoData::CurrentT() const
{
    return m_fCurrentT;
}

void ProtoData::setCurrentT(const float &arg)
{
    if (qAbs(m_fCurrentT - arg) > 1e-10) {
        m_fCurrentT = arg;
        emit CurrentTChanged();
    }
}

float ProtoData::PresetT() const
{
    return m_fPresetT;
}

void ProtoData::setPresetT(const float &arg)
{
    if (qAbs(m_fPresetT - arg) > 1e-10) {
        m_fPresetT = arg;
        emit PresetTChanged();
    }
}
