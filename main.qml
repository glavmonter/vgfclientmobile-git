import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1
import QtQuick.Window 2.1
import "content"

ApplicationWindow {
    visible: true
    width: 800
    height: 1000

    Rectangle {
        color: "#212126"
        anchors.fill: parent
    }

    toolBar: BorderImage {
        border.bottom: 8
        source: "images/toolbar.png"
        width: parent.width
        height: 80

        Text {
            font.pixelSize: 38
            anchors.verticalCenter: parent.verticalCenter
            Behavior on x { NumberAnimation { easing.type: Easing.OutCubic} }
            x: 20
            color: "white"
            text: qsTr("Клиент печи")
        }
    }

    TabView {
        anchors.fill: parent
        style: touchStyle

        Tab {
            title: qsTr("Сеть")
            TabNetworkPage {
                visible: true
            }
        }

        Tab {
            title: qsTr("Профиль")
            TabGraphicPage {
                visible: true
            }
        }

        Tab {
            title: qsTr("Зоны")
            TabZonesPage {
                visible: true
            }
        }

        Tab {
            title: qsTr("Остальное")
            TabMiscPage {
                visible: true
            }
        }

//        Tab {
//            title: qsTr("Тестовая")
//            TabTestPage {
//                visible: true
//            }
//        }
    }

    Component {
        id: touchStyle

        TabViewStyle {
            tabsAlignment: Qt.AlignVCenter
            tabOverlap: 0
            frame: Item {}

            tab: Item {
                implicitWidth: control.width/control.count
                implicitHeight: 50

                BorderImage {
                    anchors.fill: parent
                    border.bottom: 8
                    border.top: 8
                    source: styleData.selected ? "../images/tab_selected.png" : "../images//tabs_standard.png"
                    Text {
                        anchors.centerIn: parent
                        color: "white"
                        text: styleData.title.toUpperCase()
                        font.pixelSize: 16
                    }

                    Rectangle {
                        visible: index > 0
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        anchors.margins: 10
                        width: 1
                        color: "#3a3a3a"
                    }
                }
            }
        }
    }
}
