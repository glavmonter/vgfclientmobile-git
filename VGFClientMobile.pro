TEMPLATE = app

QT += widgets qml quick sensors svg network
CONFIG += debug_and_release c++11

SOURCES += main.cpp \
    plotfurnace.cpp \
    protodata.cpp \
    mainlogic.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/AndroidManifest.xml \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    plotfurnace.h \
    protodata.h \
    mainlogic.h


PROTOS +=   ../protos/protocol_tcp.proto \
            ../protos/VGFThermo.proto

include (protobuf.pri)


android {
    INCLUDEPATH += /opt/qwt-6.1.2/include
    DEPENDPATH += /opt/qwt-6.1.2/include

    QWT_PATH = /libs/armeabi-v7a
    qwt.path = /libs/armeabi-v7a
    INSTALLS += qwt
    LIBS += -L$$QWT_PATH -lqwt

} else:unix {
#    LIBS += -L/opt/qwt-6.1.2/lib -lqwt
#    INCLUDEPATH += /opt/qwt-6.1.2/include

    LIBS += -lprotobuf

    INCLUDEPATH += /usr/include/qwt6-qt5/
    DEPENDPATH += /usr/include/qwt6-qt5
    LIBS += -lqwt-qt5

} else: win32 {

    INCLUDEPATH += C:/Qwt-6.1.2/include
    INCLUDEPATH += "C:/protobuf/include"
    DEPENDPATH += C:/Qwt-6.1.2/include

    QWT_PATH = C:/Qwt-6.1.2/lib
    CONFIG(debug, debug|release) {
        # debug
        LIBS += -L$$QWT_PATH -lqwtd
        LIBS += -L"C:/protobuf/libd" -llibprotobuf
    } else {
        #release
        LIBS += -L$$QWT_PATH -lqwt
        LIBS += -L"C:/protobuf/lib" -llibprotobuf
    }
#    DEFINES    += QWT_DLL
}


