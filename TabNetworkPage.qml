import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import Qt.vgf.vgflogic 1.0


Item {
    anchors.fill: parent

    Label {
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Не реализовано")
        font.pixelSize: 30
        color: "white"
    }
}

