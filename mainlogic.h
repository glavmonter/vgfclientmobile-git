#ifndef MAINLOGIC
#define MAINLOGIC


#include <QObject>
#include <QDebug>
#include <QTimer>
#include <QSettings>
#include <QTcpSocket>

#include "protodata.h"
#include "protocol_tcp.pb.h"
#include "VGFThermo.pb.h"

class MainLogic : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ipAddress READ ipAddress WRITE setIpAddress NOTIFY ipAddressChanged)

    /* Модель для отображение параметров зоны */
    Q_PROPERTY(QVariant ModelZone READ ModelZone NOTIFY ModelZoneChanged)
    Q_PROPERTY(bool connected READ connected WRITE setConnected NOTIFY connectedChanged)

public:
    MainLogic(QObject *parent = 0);
    ~MainLogic();

    Q_INVOKABLE void switchConnectedChanged(bool checked);


private:
    QTimer *m_pTimer1s;
    QTcpSocket *m_pTcpSocket;

private slots:
    void onTimer1sTimeout();
    void slotTcpError(QAbstractSocket::SocketError arg);

public:
    QString ipAddress() const;
    void setIpAddress(const QString &arg);
signals:
    void ipAddressChanged();
private:
    QString m_sIpAddress;


public:
    QVariant ModelZone() const;
signals:
    void ModelZoneChanged();
private:
    QList<QObject *> m_lModelZone;


public:
    bool connected() const;
    void setConnected(const bool &arg);
signals:
    void connectedChanged();
private:
    bool m_bConnected = false;

};


#endif // MAINLOGIC

