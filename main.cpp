#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "plotfurnace.h"
#include "mainlogic.h"


// Second, define the singleton type provider function (callback).
static QObject *mainlogic_qobject_singletontype_provider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    MainLogic *example = new MainLogic();
    return example;
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName("MUCTR");
    QCoreApplication::setOrganizationDomain("muctr.ru");
    QCoreApplication::setApplicationName("VGFClient");

    qmlRegisterType<PlotFurnace>("QuickVgf", 1, 0, "QuickVgfFurnace");
    qmlRegisterSingletonType<MainLogic>("Qt.vgf.mainlogic", 1, 0, "MainLogic", mainlogic_qobject_singletontype_provider);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
